package nl.utwente.di.Calc;

public class CelsiusFahrenheit {
    public double getFahrenheit(double celsius) {
        return celsius * 1.8 + 32;
    }
}
