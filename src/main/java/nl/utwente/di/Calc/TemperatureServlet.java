package nl.utwente.di.Calc;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.*;
public class TemperatureServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private CelsiusFahrenheit converter;

    public void init() throws ServletException {
        converter = new CelsiusFahrenheit();
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String title = "Temperature Conversion";

        String celsiusParam = request.getParameter("celsius");
        double celsius = 0.0;
        double fahrenheit = 0.0;

        if (celsiusParam != null) {
            try {
                celsius = Double.parseDouble(celsiusParam);
                fahrenheit = converter.getFahrenheit(celsius);
            } catch (NumberFormatException e) {
                // Invalid input, you can handle the error here.
            }
        }

        out.println("<!DOCTYPE HTML>\n" +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P>Celsius: " + celsius + "\n" +
                "  <P>Fahrenheit: " + fahrenheit +
                "</BODY></HTML>");
    }
}
